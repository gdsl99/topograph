import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:topograph/model/tabelaModel.dart';
import 'package:topograph/telas/Pontos.dart';

class TabelasScreen extends StatefulWidget {
  @override
  _TabelasScreenState createState() => _TabelasScreenState();
}

class _TabelasScreenState extends State<TabelasScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final keyFormulario = GlobalKey<FormState>();
    late TextEditingController nomeTabela = TextEditingController();

    Future<List<Tabela>> getTabelas() async {
      final response = await http.get(Uri.parse(
          "https://615f740ef7254d001706814a.mockapi.io/topograph/tabela"));
      final List<dynamic> responseMap = jsonDecode(response.body);
      return responseMap.map<Tabela>((resp) => Tabela.fromMap(resp)).toList();
    }

    Future<List<Tabela>>? tabelas = getTabelas();

    void postTabela() async {
      final response = await http.post(
        Uri.parse(
            'https://615f740ef7254d001706814a.mockapi.io/topograph/tabela/'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{'nome': nomeTabela.text}),
      );
      if (response.statusCode == 404) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Erro ao cadastrar')),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Tabela cadastrada')),
        );
      }
    }

    void criaNovaTabela() {
      if (keyFormulario.currentState!.validate()) {
        postTabela();
        FocusScope.of(context).unfocus();
        Navigator.pop(context);
        setState(() {});
      }
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Tabelas",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                FutureBuilder(
                  future: tabelas,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.zero,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Botao(
                              idTabelaParametro: snapshot.data[index].id,
                              nome: snapshot.data[index].nome,
                            );
                          },
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Container(
                          margin: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.4,
                          ),
                          child: Text("Erro ao carregar os dados"));
                    } else {
                      return Container(
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.4,
                        ),
                        width: 60,
                        height: 60,
                        child: CircularProgressIndicator(
                          color: Colors.white,
                        ),
                      );
                    }
                  },
                ),
              ],
            ),
          )),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        onPressed: () => showDialog<String>(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            title: Text(
              'Adicionar nova tabela',
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              Form(
                key: keyFormulario,
                child: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Campo vazio!';
                    }
                    return null;
                  },
                  controller: nomeTabela,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: OutlineInputBorder(),
                      labelText: "Nome da tabela",
                      labelStyle: TextStyle(color: Colors.black)),
                ),
              ),
              TextButton(
                onPressed: () => criaNovaTabela(),
                child: Text(
                  'OK',
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Botao extends StatefulWidget {
  const Botao({Key? key, required this.nome, required this.idTabelaParametro})
      : super(key: key);
  final String nome;
  final String idTabelaParametro;

  @override
  _BotaoState createState() => _BotaoState();
}

class _BotaoState extends State<Botao> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: TextButton(
          style: TextButton.styleFrom(
            splashFactory: NoSplash.splashFactory,
            padding: EdgeInsets.only(left: 20),
            alignment: Alignment.centerLeft,
            side: BorderSide(
              width: 1.5,
              color: Colors.white,
            ),
            fixedSize: Size(MediaQuery.of(context).size.width * 0.9, 60),
            textStyle: const TextStyle(fontSize: 20),
          ),
          onPressed: () async {
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            PontosScreen(idTabela: widget.idTabelaParametro)))
                .then((value) {
              setState(() {});
            });
          },
          child: Text(
            widget.nome,
            style: TextStyle(color: Colors.white),
          )),
    );
  }
}
