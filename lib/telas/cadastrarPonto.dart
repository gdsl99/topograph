import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:topograph/Widgets/TextField.dart';
import 'package:http/http.dart' as http;

class CadastrarPontoScreen extends StatefulWidget {
  const CadastrarPontoScreen({Key? key}) : super(key: key);

  @override
  _CadastrarPontoScreenState createState() => _CadastrarPontoScreenState();
}

class _CadastrarPontoScreenState extends State<CadastrarPontoScreen> {
  final _formKey = GlobalKey<FormState>();

  late TextEditingController tnomePonto = TextEditingController(),
      tpontoSuperior = TextEditingController(),
      tre1 = TextEditingController(),
      tre2 = TextEditingController(),
      tre3 = TextEditingController(),
      tvante1 = TextEditingController(),
      tvante2 = TextEditingController(),
      tvante3 = TextEditingController(),
      tx = TextEditingController(),
      ty = TextEditingController(),
      tdistanciaRe = TextEditingController(),
      tdistanciaVante = TextEditingController(),
      tdesivel = TextEditingController();

  postPonto() async {
    tpontoSuperior.text = tvante3.text;
    final response = await http.post(
      Uri.parse(
          'https://615f740ef7254d001706814a.mockapi.io/topograph/tabela/1/ponto/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
          <String, String>{'nomePonto': tnomePonto.text, 're1': tre1.text}),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Novo Ponto"),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration:
            BoxDecoration(border: Border.all(color: Colors.red, width: 1)),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.9,
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFieldCustom(
                        myController: tnomePonto,
                        textType: TextInputType.name,
                        unlocked: true,
                        label: "Nome do ponto",
                      ),
                      TextFieldCustom(
                        myController: tre1,
                        textType:
                            TextInputType.numberWithOptions(decimal: true),
                        unlocked: true,
                        label: "Ré 1",
                      ),
                      TextFieldCustom(
                        myController: tre2,
                        textType:
                            TextInputType.numberWithOptions(decimal: true),
                        unlocked: true,
                        label: "Ré 2",
                      ),
                      TextFieldCustom(
                        myController: tre3,
                        textType:
                            TextInputType.numberWithOptions(decimal: true),
                        unlocked: true,
                        label: "Ré 3",
                      ),
                      TextFieldCustom(
                        myController: tvante1,
                        textType:
                            TextInputType.numberWithOptions(decimal: true),
                        unlocked: true,
                        label: "Vante 1",
                      ),
                      TextFieldCustom(
                        myController: tvante2,
                        textType:
                            TextInputType.numberWithOptions(decimal: true),
                        unlocked: true,
                        label: "Vante 2",
                      ),
                      TextFieldCustom(
                        myController: tvante3,
                        textType:
                            TextInputType.numberWithOptions(decimal: true),
                        unlocked: true,
                        label: "Vante 3",
                      ),
                      TextFieldCustom(
                        myController: tx,
                        textType:
                            TextInputType.numberWithOptions(decimal: true),
                        unlocked: true,
                        label: "X*",
                      ),
                      TextFieldCustom(
                        myController: ty,
                        textType:
                            TextInputType.numberWithOptions(decimal: true),
                        unlocked: true,
                        label: "Y*",
                      ),
                      TextFieldCustom(
                        myController: tpontoSuperior,
                        textType: TextInputType.name,
                        unlocked: true,
                        label: "Ponto superior*",
                      ),
                      TextFieldCustom(
                        myController: tdistanciaRe,
                        textType: TextInputType.name,
                        unlocked: true,
                        label: "Distancia Re*",
                      ),
                      TextFieldCustom(
                        myController: tdistanciaVante,
                        textType: TextInputType.name,
                        unlocked: true,
                        label: "Distancia vante*",
                      ),
                      TextFieldCustom(
                        myController: tdesivel,
                        textType: TextInputType.name,
                        unlocked: true,
                        label: "Desnivel*",
                      ),
                      ElevatedButton(
                        onPressed: () {
                          // Validate returns true if the form is valid, or false otherwise.
                          // If the form is valid, display a snackbar. In the real world,
                          // you'd often call a server or save the information in a database.
                          postPonto();
                        },
                        child: const Text('Submit'),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
