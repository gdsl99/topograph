import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:topograph/model/pontoModel.dart';

class PontosScreen extends StatefulWidget {
  PontosScreen({Key? key, required this.idTabela}) : super(key: key);

  final String idTabela;

  @override
  _PontosScreenState createState() => _PontosScreenState();
}

class _PontosScreenState extends State<PontosScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future<List<Ponto>> getPontos() async {
      final response = await http.get(Uri.parse(
          "https://615f740ef7254d001706814a.mockapi.io/topograph/tabela/1/ponto"));
      final List<dynamic> responseMap = jsonDecode(response.body);
      print(responseMap.map<Ponto>((resp) => Ponto.fromMap(resp)).toList());
      return responseMap.map<Ponto>((resp) => Ponto.fromMap(resp)).toList();
    }

    Future<List<Ponto>> pontos = getPontos();
    print(pontos);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Pontos",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              FutureBuilder(
                future: pontos,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    return Container(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        padding: EdgeInsets.zero,
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Botao(
                            distanciaRe: snapshot.data[index].distanciaRe,
                            nomePonto: snapshot.data[index].nomePonto,
                            vante1: snapshot.data[index].vante1,
                            vante2: snapshot.data[index].vante2,
                            vante3: snapshot.data[index].vante3,
                            desnivel: snapshot.data[index].desnivel,
                            distanciaVante: snapshot.data[index].distanciaVante,
                            re1: snapshot.data[index].re1,
                            re2: snapshot.data[index].re2,
                            re3: snapshot.data[index].re3,
                            x: snapshot.data[index].x,
                            y: snapshot.data[index].y,
                            pontoSuperior: snapshot.data[index].pontoSuperior,
                          );
                        },
                      ),
                    );
                  } else if (snapshot.hasError) {
                    return Container(
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.4,
                        ),
                        child: Text("Erro ao carregar os dados"));
                  } else {
                    return Container(
                      margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.4,
                      ),
                      width: 60,
                      height: 60,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Botao extends StatelessWidget {
  const Botao(
      {Key? key,
      required this.nomePonto,
      required this.vante2,
      required this.vante1,
      required this.vante3,
      required this.re1,
      required this.re2,
      required this.re3,
      required this.distanciaVante,
      required this.desnivel,
      required this.x,
      required this.y,
      required this.distanciaRe,
      required this.pontoSuperior})
      : super(key: key);
  final String nomePonto;
  final int vante1;
  final int vante2;
  final int vante3;
  final int re1;
  final int re2;
  final int re3;
  final int distanciaVante;
  final int distanciaRe;
  final int desnivel;
  final int x;
  final int y;
  final String pontoSuperior;

  @override
  Widget build(BuildContext context) {
    Widget textCustomizado(texto) {
      return Text(
        texto,
        style: TextStyle(color: Colors.white, fontSize: 18),
      );
    }

    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(border: Border.all(color: Colors.white)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textCustomizado('Nome do ponto'),
              textCustomizado(nomePonto)
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textCustomizado("Vante"),
              textCustomizado(vante1.toString() +
                  " - " +
                  vante2.toString() +
                  " - " +
                  vante3.toString())
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textCustomizado("Re"),
              textCustomizado(re1.toString() +
                  " - " +
                  re2.toString() +
                  " - " +
                  re3.toString())
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textCustomizado("Distancia Re"),
              textCustomizado(distanciaRe.toString())
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textCustomizado("Distancia Vante"),
              textCustomizado(distanciaVante.toString())
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              textCustomizado("Ponto superior"),
              textCustomizado(pontoSuperior)
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [textCustomizado("X"), textCustomizado(x.toString())],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [textCustomizado("Y"), textCustomizado(y.toString())],
          ),
        ],
      ),
    );
  }
}
