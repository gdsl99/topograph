import 'dart:convert';

class Tabela {
  late String id;
  late String nome;

  Tabela({
    required this.id,
    required this.nome,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nome': nome,
    };
  }

  factory Tabela.fromMap(Map<String, dynamic> map) {
    return Tabela(
      id: map['id'],
      nome: map['nome'],
    );
  }
  String toJson() => json.encode(toMap());

  factory Tabela.fromJson(String source) => Tabela.fromMap(json.decode(source));
}
