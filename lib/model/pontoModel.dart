import 'dart:convert';

class Ponto {
  late String id;
  late String tabelaId;
  late String pontosVisados;
  late String pontoSuperior;
  late int distanciaRe;
  late int vante1;
  late int vante2;
  late int vante3;
  late int re1;
  late int re2;
  late int re3;
  late int distanciaVante;
  late int desnivel;
  late String nomePonto;
  late int x;
  late int y;

  Ponto({
    required this.id,
    required this.tabelaId,
    required this.pontosVisados,
    required this.distanciaRe,
    required this.vante1,
    required this.vante2,
    required this.vante3,
    required this.re1,
    required this.re2,
    required this.re3,
    required this.distanciaVante,
    required this.desnivel,
    required this.nomePonto,
    required this.x,
    required this.y,
    required this.pontoSuperior,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'tabelaId': tabelaId,
      'pontoSuperior': pontoSuperior,
      'pontosVisados': pontosVisados,
      'distanciaRe': distanciaRe,
      'vante1': vante1,
      'vante2': vante2,
      'vante3': vante3,
      're1': re1,
      're2': re2,
      're3': re3,
      'distanciaVante': distanciaVante,
      'desnivel': desnivel,
      'nomePonto': nomePonto,
      'x': x,
      'y': y,
    };
  }

  factory Ponto.fromMap(Map<String, dynamic> map) {
    return Ponto(
      id: map['id'],
      tabelaId: map['tabelaId'],
      pontoSuperior: map['pontoSuperior'],
      pontosVisados: map['pontosVisados'],
      distanciaRe: map['distanciaRe'],
      vante1: map['vante1'],
      vante2: map['vante2'],
      vante3: map['vante3'],
      re1: map['re1'],
      re2: map['re2'],
      re3: map['re3'],
      distanciaVante: map['distanciaVante'],
      desnivel: map['desnivel'],
      nomePonto: map['nomePonto'],
      x: map['x'],
      y: map['y'],
    );
  }
  String toJson() => json.encode(toMap());

  factory Ponto.fromJson(String source) => Ponto.fromMap(json.decode(source));
}
