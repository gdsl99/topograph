import 'package:flutter/material.dart';
import 'package:topograph/telas/Pontos.dart';
import 'package:topograph/telas/Tabelas.dart';
import 'package:topograph/telas/cadastrarPonto.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          appBarTheme: AppBarTheme(
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            color: Colors.white,
            titleTextStyle: TextStyle(),
          ),
          scaffoldBackgroundColor: Colors.grey[800]),
      home: TabelasScreen(),
    );
  }
}
