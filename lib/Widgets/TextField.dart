import 'package:flutter/material.dart';

class TextFieldCustom extends StatefulWidget {
  const TextFieldCustom(
      {Key? key,
      required this.myController,
      required this.label,
      required this.unlocked,
      required this.textType})
      : super(key: key);
  final String label;
  final TextEditingController myController;
  final bool unlocked;
  final TextInputType textType;
  @override
  _TextFieldCustomState createState() => _TextFieldCustomState();
}

class _TextFieldCustomState extends State<TextFieldCustom> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: TextFormField(
        controller: widget.myController,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Campo vazio!';
          }
          return null;
        },
        keyboardType: widget.textType,
        enabled: widget.unlocked,
        style: TextStyle(color: Colors.white),
        cursorColor: Colors.white,
        decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            border: OutlineInputBorder(),
            labelText: widget.label,
            labelStyle: TextStyle(color: Colors.white54)),
      ),
    );
  }
}
